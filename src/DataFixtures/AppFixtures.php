<?php

namespace App\DataFixtures;

use App\Entity\Entry;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var \Faker\Factory
     */
    private $faker;
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->faker = \Faker\Factory::create();
        $this->encoder = $encoder;
    }


    public function load(ObjectManager $manager)
    {
        self::FakeAuthors($manager);
        self::FakeEntries($manager);
        $manager->flush();
    }

    public function FakeAuthors(ObjectManager $manager)
    {
        $totalAuthors = 10;
        for ($i = 0; $i < $totalAuthors; $i++) {
            $email = $this->faker->email();
            $username = $this->faker->userName();
            $twitterUser = $this->faker->userName();
            $password = $this->faker->password();
            $user = new User();
            $user->setEmail($email)
                ->setUsername($username)
                ->setTwitterUser($twitterUser)
                ->setPassword($this->encoder->encodePassword($user, $password));

            $manager->persist($user);
        }
        $manager->flush();
    }

    public function FakeEntries(ObjectManager $manager)
    {
        $authors = $manager->getRepository(User::class)->findAll();
        $totalEntries = 20;
        for ($i=0; $i < $totalEntries; $i++) {
            $entry = new Entry();
            $entry->setAuthor($this->faker->randomElement($authors));
            $entry->setTitle($this->faker->realText($maxNbChars = 20));
            $entry->setContent($this->faker->realText($maxNbChars = 200, $indexSize = 2));
            $entry->setCreationDate($this->faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'));
            $manager->persist($entry);
        }
        $manager->flush();
    }
}
