<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\DBALException;
use mysql_xdevapi\Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    /** EntityManager $manager */
    private $manager;
    /** UserPasswordEncoderInterface $encoder */
    private $encoder;

    /**
     * UserRepository constructor.
     * @param ManagerRegistry $registry
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(ManagerRegistry $registry, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct($registry, User::class);
        $this->manager = $registry->getEntityManager();
        $this->encoder = $encoder;
    }

    /**
     * Create a new user
     * @param $data
     * @return User
     */
    public function createNewUser($data)
    {
        $user = new User();
        $user->setEmail($data['email'])
            ->setUsername($data['username'])
            ->setTwitterUser($data['twitterUser'])
            ->setPassword($this->encoder->encodePassword($user, $data['password']));

        $this->manager->persist($user);
        $this->manager->flush();

        return $user;
    }
}
