<?php

namespace App\Repository;

use App\Entity\UserTweet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserTweet|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserTweet|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserTweet[]    findAll()
 * @method UserTweet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserTweetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserTweet::class);
    }

    // /**
    //  * @return UserTweet[] Returns an array of UserTweet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserTweet
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
