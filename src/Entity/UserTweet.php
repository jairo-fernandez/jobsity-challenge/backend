<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"User"}},
 *     collectionOperations={
 *         "get",
 *         "post"={"security_post_denormalize"="object.getUser() == user"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security_post_denormalize"="is_granted('ROLE_USER') and object.getUser() == user and previous_object.getUser()"}
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserTweetRepository")
 */
class UserTweet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"User"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tweetsStatus")
     */
    private $user;

    /**
     * @Serializer\Groups({"User"})
     * @ORM\Column(type="string", length=255)
     */
    private $tweetId;

    /**
     * @Serializer\Groups({"User"})
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTweetId(): ?string
    {
        return $this->tweetId;
    }

    public function setTweetId(string $tweetId): self
    {
        $this->tweetId = $tweetId;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
