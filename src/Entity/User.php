<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"User"}},
 *     collectionOperations={
 *         "get",
 *         "post"={"security_post_denormalize"="object == user"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security_post_denormalize"="is_granted('ROLE_USER') and object == user and previous_object.getId() == user.getId() and (previous_object.getRoles() == user.getRoles() or is_granted('ROLE_ADMIN') )"}
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"User"})
     */
    private $id;

    /**
     * @Serializer\Groups({"User", "Entry"})
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @Serializer\Groups({"User"})
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"User"})
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"User", "Entry"})
     */
    private $twitterUser;

    /**
     * @Serializer\Groups({"User"})
     * @ORM\OneToMany(targetEntity="Entry", mappedBy="author")
     */
    private $entries;

    /**
     * @Serializer\Groups({"User"})
     * @ORM\OneToMany(targetEntity="UserTweet", mappedBy="user")
     */
    private $tweetsStatus;

    public function __construct()
    {
        $this->entries = new ArrayCollection();
        $this->tweetsStatus = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTwitterUser(): ?string
    {
        return $this->twitterUser;
    }

    public function setTwitterUser(?string $twitterUser): self
    {
        $this->twitterUser = $twitterUser;

        return $this;
    }

    /**
     * @return Collection|Entry[]
     */
    public function getEntries(): Collection
    {
        return $this->entries;
    }

    public function addEntry(Entry $entry): self
    {
        if (!$this->entries->contains($entry)) {
            $this->entries[] = $entry;
            $entry->setAuthor($this);
        }

        return $this;
    }

    public function removeEntry(Entry $entry): self
    {
        if ($this->entries->contains($entry)) {
            $this->entries->removeElement($entry);
            // set the owning side to null (unless already changed)
            if ($entry->getAuthor() === $this) {
                $entry->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserTweet[]
     */
    public function getTweetsStatus(): Collection
    {
        return $this->tweetsStatus;
    }

    public function addTweetsStatus(UserTweet $tweetsStatus): self
    {
        if (!$this->tweetsStatus->contains($tweetsStatus)) {
            $this->tweetsStatus[] = $tweetsStatus;
            $tweetsStatus->setUser($this);
        }

        return $this;
    }

    public function removeTweetsStatus(UserTweet $tweetsStatus): self
    {
        if ($this->tweetsStatus->contains($tweetsStatus)) {
            $this->tweetsStatus->removeElement($tweetsStatus);
            // set the owning side to null (unless already changed)
            if ($tweetsStatus->getUser() === $this) {
                $tweetsStatus->setUser(null);
            }
        }

        return $this;
    }
}
