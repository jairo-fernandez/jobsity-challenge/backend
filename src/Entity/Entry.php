<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"Entry"}},
 *     collectionOperations={
 *         "get",
 *         "post"={"security_post_denormalize"="is_granted('ROLE_USER') and object.getAuthor() == user"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security_post_denormalize"="is_granted('ROLE_USER') and object.getAuthor() == user and previous_object.getAuthor() == user"}
 *     }
 * )
 * @ApiFilter(OrderFilter::class)
 * @ORM\Entity(repositoryClass="App\Repository\EntryRepository")
 */
class Entry
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"Entry"})
     */
    private $id;

    /**
     * @Serializer\Groups({"Entry", "User"})
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @Serializer\Groups({"Entry", "User"})
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @Assert\NotBlank
     * @Serializer\Groups({"Entry"})
     * @ORM\ManyToOne(targetEntity="User", inversedBy="entries")
     */
    private $author;

    /**
     * @Serializer\Groups({"Entry"})
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }
}
