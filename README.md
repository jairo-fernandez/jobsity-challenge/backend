## PHP Jobsity challenge
Demo:
[https://jairo-fernandez-jobsity.herokuapp.com/api](https://jairo-fernandez-jobsity.herokuapp.com/api)

### Instructions

Set an environmnent
```bash
cp .env.dist .env
```

Create a key pem for JWT tokens
```bash
# For pass please use the value of ENV JWT_PASSPHRASE
mkdir -p config/jwt
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
chmod -R 644 config/jwt/*
```

This project supports docker 🐳
```bash
docker compose build
docker compose up -d
```

The `docker-compose.yml` describes the infrastructure with simple services:
- apache: The web server
- mysql: The engine db
- php: The server language 😎
- phpmyadmin: The admin for db, used only for dev purpose ⚠🧐️☢️

For example if you need to access to mysql or php container
```bash
docker exec -it sf4_mysql bash
docker exec -it -u dev sf4_php bash
```

### Register and Login
```bash
# Register
curl --request POST \
  --url 'http://jairo-fernandez.jobsitychallenge.com/register?username=jairo&email=jairo@mail.com&password=test1&twitterUser=@jairo' \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json'
```

```bash
# Login
curl --request POST \
  --url http://jairo-fernandez.jobsitychallenge.com/login \
  --header 'Content-Type: application/json' \
  --data '{ "username": "jairo", "password": "test1"}'
```


#### Env for development
Allow a virtual host in local machine for dev
```bash
nano /etc/hosts # vim /etc/hosts
```
Add the next line in the file `/etc/hosts`
```bash
127.0.1.1       jairo-fernandez.jobsitychallenge.com
```

The precise config for apache is in `.docker/config/vhosts/sf4.conf`
